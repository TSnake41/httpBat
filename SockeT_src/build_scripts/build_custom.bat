@Echo off
mkdir bin

if [%~1]==[] goto :exit

cd ..

set PATH=%PATH%;%windir%\Microsoft.NET\Framework\v3.5

REM En attendant que Microsoft corrige le bug
set PATH=%PATH%;%windir%\Microsoft.NET\Framework\v3.5

csc /o+ /out:build_scripts\bin\SockeT.exe *.cs /define:CLIENT;SERVER;TCP;NMS;HELP
pause
exit /b

:exit
echo SockeT, custom building tool
echo Usage : build_custom.bat MODULE1;MODULE2;MODULE3;MODULEX
echo Examples : 
echo build_custom.bat SERVER;NMS -^> Build a minimal NMS server SockeT.exe
echo build_custom.bat CLIENT;TCP -^> Build a minimal TCP client SockeT.exe
cd build_scripts