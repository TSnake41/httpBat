﻿/* This file is a part of SockeT and licenced under the MIT licence
* For more informations, see LICENCE or choosealicense.com/licenses/mit/
*/

#if SERVER
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace SockeT
{
    public class Server : Module
    {
        public Socket Listner;

        string Protocol;

        Thread AcceptLoop;

        List<Session> Sessions = new List<Session>();

        public void Init()
        {
            Bind(Program.properties["host"], int.Parse(Program.properties["port"]));

            if (Program.properties.ContainsKey("protocol") && !string.IsNullOrEmpty(Program.properties["protocol"]))
                Protocol = Program.properties["protocol"].ToLower();
            else
                Protocol = "tcp";
        }

        public void Bind(string host, int port)
        {
            var entry = new IPEndPoint(IPAddress.Parse(host), port);

            Listner = new Socket(entry.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            Listner.Bind(entry);
            #if DEBUG
            Debug.Write("[SERVER] Listner binded to {0}:{1}", host, port);
            #endif

            Listner.Listen(2);
            #if DEBUG
            Debug.Write("[SERVER] Listner is listening");
            #endif
        }

        public void Start()
        {
            Directory.CreateDirectory("SockeT");
            
            AcceptLoop = new Thread(() =>
            {
                while (true)
                {
                    Socket client = null;

                    while (Sessions.Count >= Program.MaxSessions)
                        Thread.Sleep(Program.WaitDelay);

                    try
                    {
                        client = Listner.Accept();
                    }
                    catch { continue; }

                    var clientEndPoint = (IPEndPoint)client.RemoteEndPoint;

                    #if (DEBUG)
                    Debug.Write("[SERVER] Connection accepted for {0}.", client.RemoteEndPoint.ToString());
                    #endif

                    string file = string.Format("SockeT\\{0}${1}", clientEndPoint.Address, clientEndPoint.Port);

                    File.Create(file).Close();

                    ThreadStart Void = () => {};

                    Thread sender, reciever;
                    Thread checker = sender = reciever = new Thread(Void);

                    checker = new Thread(session =>
                    {
                        File.Create(file).Close();

                        while (client.Connected)
                        {
                            if (!File.Exists(file))
                                break;

                            Thread.Sleep(Program.WaitDelay);
                        }

                        Sessions.Remove((Session)session);
                        ((Session)session).Dispose();
                    });

                    string InFile = string.Concat(file, "_IN");
                    string OutFile = string.Concat(file, "_OUT");

                    // Create threads
                    #if TCP
                    if (Protocol.ToLower() == "tcp")
                    {
                        sender = new Thread(() => TCP.Sender(client, InFile));
                        reciever = new Thread(() => TCP.Reciever(client, OutFile));
                    }
                    #endif
                    #if NMS
                    // NMS : 16 bits length + "length" octect datas.
                    if (Protocol.ToLower() == "nms")
                    {
                        sender = new Thread(() => NMS.Sender(client, InFile));
                        reciever = new Thread(() => NMS.Reciever(client, OutFile));
                    }
                    #endif

                    var session2 = new Session(client, checker, sender, reciever, file);

                    #if (DEBUG)
                    Debug.Write("[SERVER] Start sender, reciever and checker threads for {0}", clientEndPoint.ToString());
                    #endif
                    sender.Start();
                    reciever.Start();
                    checker.Start(session2);

                    #if (DEBUG)
                    Debug.Write("[SERVER] Session {0} added for {1}", Sessions.Count + 1, clientEndPoint.ToString());
                    #endif
                    Sessions.Add(session2);
                }
            });
            AcceptLoop.Start();
        }

        public void Stop()
        {
            #if DEBUG
            Debug.Write("[SERVER] Server stopped");
            #endif

            AcceptLoop.Abort();

            foreach (var session in Sessions)
                session.Dispose();

            Sessions.Clear();

            Listner.Close();
            try { Directory.Delete("SockeT", true); } catch { }
        }

        public bool Active()
        {
            return Listner.IsBound;
        }
    }
}
#endif