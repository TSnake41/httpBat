﻿/* This file is a part of SockeT and licenced under the MIT licence
 * For more informations, see LICENCE or choosealicense.com/licenses/mit/
 */

#if CLIENT
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace SockeT
{
    public class Client : Module
    {
        Socket client;

        IPEndPoint entry;
        string Protocol;

        Thread reciever, sender;

        public void Init()
        {
            Link(Program.properties["host"], int.Parse(Program.properties["port"]));

            if (Program.properties.ContainsKey("protocol") && !string.IsNullOrEmpty(Program.properties["protocol"]))
                Protocol = Program.properties["protocol"].ToLower();
            else
                Protocol = "tcp";
        }

        public void Link(string host, int port)
        {
            #if DEBUG
            Debug.Write("[CLIENT] Link the client to {0}:{1}", host, port);
            #endif

            entry = new IPEndPoint(IPAddress.Parse(host), port);

            client = new Socket(entry.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        }

        public void Start()
        {
            client.Connect(entry);

            // Create threads
            #if TCP
            if (Protocol.ToLower() == "tcp")
            {
                sender = new Thread(() => TCP.Sender(client, "IN"));
                reciever = new Thread(() => TCP.Reciever(client, "OUT"));
            }
            #endif
            #if NMS
            // NMS : 16 bits length + "length" octect datas.
            if (Protocol.ToLower() == "nms")
            {
                sender = new Thread(() => NMS.Sender(client, "IN"));
                reciever = new Thread(() => NMS.Reciever(client, "OUT"));
            }
            #endif

            sender.Start();
            reciever.Start();

            #if DEBUG
            Debug.Write("[CLIENT] Protocol {0} started", Protocol);
            #endif
        }

        public void Stop()
        {
            #if DEBUG
            Debug.Write("[CLIENT] Client stopped");
            #endif

            
            if (sender != null) sender.Abort();
            if (reciever != null) reciever.Abort();

            if (client != null && client.Connected)
                client.Disconnect(false);

            File.Delete("IN");
            File.Delete("OUT");
        }

        public bool Active()
        {
            if (client == null || !client.Connected)
            {
                System.Console.WriteLine("Connection lost");
                return false;
            }

            return true;
        }
    }
}
#endif