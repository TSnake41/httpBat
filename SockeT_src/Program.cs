﻿// SockeT Reloaded

/* Modules List :
 * CLIENT - Complete
 * SERVER - Complete
 * SERVER_MANAGEMENT
 * HELP - Complete
 * TCP - Complete
 * NMS - Complete
 * DEBUG
 * 
 * Parameters define : VERBOSE : show verbose DEBUG
 */

/* This file is a part of SockeT and licenced under the MIT licence
* For more informations, see LICENCE or choosealicense.com/licenses/mit/
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Threading;

namespace SockeT
{
    public class Program
    {
        public static Dictionary<string, Module> modules = new Dictionary<string, Module>
        {
            #if CLIENT
            { "client", new Client() },
            #endif

            #if SERVER
            { "server", new Server() },
            #endif
        };

        public static Dictionary<string, string> properties = new Dictionary<string, string>();
        public static int WaitDelay;
        public static int MaxSessions;

        static void Main(string[] args)
        {
            try
            {
                // Argument parsing.
                Array.ForEach(args, arg =>
                {
                    arg = arg.Remove(0, 1);
                    if (string.IsNullOrEmpty(arg))
                        return;

                    string property = arg.Split(new[] { ':' }, 2)[0].ToLower();
                    string value = arg.Split(new[] { ':' }, 2)[1].ToLower();
                    properties.Add(property, value);
                });

                if (!properties.ContainsKey("m"))
                {
                    #if HELP
                    ShowHelp();
                    #endif
                    return;
                }


                if (!(properties.ContainsKey("waitdelay") && int.TryParse(properties["waitdelay"], out WaitDelay)))
                    WaitDelay = 50;

                if (!(properties.ContainsKey("maxclients") && int.TryParse(properties["maxclients"], out MaxSessions)))
                    MaxSessions = 10;
									
                var module = modules[properties["m"]];
                module.Init();

                var sessionThread = new Thread(() =>
                {
                    File.Create("Session").Close();
                    Thread.Sleep(WaitDelay);
                    while (module.Active() && File.Exists("Session"))
                        Thread.Sleep(WaitDelay);

                    File.Delete("Session");
                    #if DEBUG
                    Debug.Write("Session deleted, modules stopped, process ended");
                    #endif
                    module.Stop();
                    Environment.Exit(2);
                });
                sessionThread.Start();

                module.Start();
            }
            catch (SocketException)
            {
                Console.WriteLine("Connection End or linking failed");
                #if DEBUG
                Debug.Write("[Error] SocketException, process ended");
                #endif
            }
        }

        #if HELP
        static void ShowHelp()
        {
            Console.WriteLine("SockeT.exe by Astie Teddy");
            Console.WriteLine("Usage : SockeT -property:value [...]");
            Console.WriteLine("\nPrimary properties : ");
            Console.WriteLine("m : Module to use (client/server)");
            Console.WriteLine("host : listening/connecting ip address");
            Console.WriteLine("port : binding port / connecting port");
            Console.WriteLine("\nInstalled modules : ");
            Console.WriteLine("HELP");
            #if SERVER
            Console.WriteLine("SERVER");
            #endif
            #if CLIENT
            Console.WriteLine("CLIENT");
            #endif
            #if TCP
            Console.WriteLine("TCP");
            #endif
            #if NMS
            Console.WriteLine("NMS");
            #endif
        }
        #endif
    }
}