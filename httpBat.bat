@echo off
if /i "%~1" EQU "Thread" (
	title; 
	setlocal enabledelayedexpansion
	call :connection
	del /s /q "!FILE!_PAUSE"> nul 2>&1
	exit)
REM  Copyright (C) 2016-2018 Astie Teddy (TSnake41)
REM  Copyright (C) 2016-2018 IK-DC Xenosis
REM
REM  Permission is hereby granted, free of charge, to any person obtaining
REM  a copy of this software and associated documentation files (the
REM  "Software"), to deal in the Software without restriction, including
REM  without limitation the rights to use, copy, modify, merge, publish,
REM  distribute, sublicense, and/or sell copies of the Software, and to
REM  permit persons to whom the Software is furnished to do so, subject to
REM  the following conditions:
REM
REM  The above copyright notice and this permission notice shall be
REM  included in all copies or substantial portions of the Software.
REM
REM  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
REM  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
REM  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
REM  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
REM  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
REM  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
REM  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
:: Acc‚s par HTTP (firefox, chrome, edge, etc.) = http://127.0.0.1:%PORT%
set PATH=%CD%;%PATH%
set basedir=%cd%

echo;--- HttpBat by TSnake41/Xenoxis ---
echo;Initialize basic codes
if exist Session call :reset

for %%A in (
	"200 OK"
	"403 Forbidden"
	"404 Not Found"
	"302 Moved Temporarily"
	"303 Moved Permanently"
	"206 Partial Content"
	"501 Not Implemented"
	) do for /f "tokens=1,*" %%B in ("%%~A") do set code_%%B=%%C

set ActuPATH=%~0
(
	if exist "Compress\*.*" rmdir /s /q "Compress"
	if exist "ErrorPages\*.*" rmdir /s /q "ErrorPages"
	if not exist http mkdir http
	if exist phphtml rmdir /s /q "phphtml"
	mkdir phphtml
	if not exist Logs mkdir Logs
	for /f "tokens=*" %%a in ('ver') do set WinOS=%%a
	if /I "%PROCESSOR_ARCHITECTURE%" EQU "x86" (set OsBits=32) else set OsBits=64
	for /f "tokens=2,*" %%a in ('Reg Query HKEY_LOCAL_MACHINE\HARDWARE\DESCRIPTION\System\CentralProcessor\0 /v ProcessorNameString 2^> nul') do set CPU=%%b
)> nul 2>&1

call :log ------------------------------------------------------------------------
call :log [%time:~0,8%] Starting HttpBat Master
setlocal enabledelayedexpansion
echo;Expansions are ON

REM Chargement des configs
call config_tool.bat load
if /i "%AllowGzip%" EQU "true" if not exist Compress mkdir Compress
if /i "%StaticCompression%" EQU "true" if /i "%AllowGzip%" EQU "true" (
	echo;Make Index for Static Compression ...
	call config_tool.bat StaticCompression
	set /A StaticCompressionReload=%StaticCompressionRefresh%
	call :log [%time:~0,8%] Static Compression index maked.
)
if /i "%MimeIndex%" EQU "true" (
	echo;Make MIME Index ...
	for /f "tokens=1,*" %%A in (mime.txt) do set MIME-%%A=%%B
	call :log [%time:~0,8%] MIME Index maked.
)
set usebatbox=true
if not exist batbox.exe (
	echo;Generate batbox.exe ...
	call :makebatbox
	if not exist batbox.exe if not exist Wait.exe (
		echo;Generate Wait.exe ...
		call :makeWait
		set usebatbox=
))

if "%usebatbox%" EQU "true" batbox /h 0

if not exist 7z.exe (
	echo;7z.exe not found, compression not supported.
	call :log [%time:~0,8%] 7z.exe not found, compression not supported.
	set AllowGzip=
)
 
echo PORT=%PORT%

if not exist SockeT.exe call :genSockeT||(
	copy SockeT_src\Precompiled\SockeT.exe SockeT.exe
)>NUL 2>&1

:: D‚marage de SockeT en mode ‚coute (accepte les connection TCP)
:: HTTP ~= TCP + Headers + Body

echo SockeT starting...
set SERVER_PORT=%PORT%
set SERVER_NAME=%IP%
set IP=
set PORT=
set GATEWAY_INTERFACE=CGI/1.1
start /b SockeT /m:server -host:%SERVER_NAME% -port:%SERVER_PORT% -maxclients:%maxInstantConnections% -waitdelay:1

:: Cette ‚tape est critique, si SockeT plante, cela ‚vite les suppr‚ssions non-voulues.
mkdir SockeT 2>nul
cd SockeT

for /l %%a in (0,1,100) do (
	if exist "..\Session" goto Started
	call :wait 100
	)
goto end
:Started
echo HttBat started ^^!
call :log [%time:~0,8%] Server correctly started, waiting clients ...
echo Waiting for clients...

:: Recherche d'un fichier d'IP (sans OUT ni IN (fichier de session)).
:: Ce fichier permet de detecter une connection sur le serveur.

:waitClient
for %%A in (*) do (
	set File=%%A
	   if not "!File:~-3!"=="OUT" if not "!File:~-2!"=="IN" if not "!File:~-6!"=="_Pause" (
				if not exist "!File!_Pause" if /i "%MultiThreadingSupport%" EQU "true" (
					echo;>!File!_Pause
					start /b /%ThreadPriority% "" call "%~0" Thread) else call :connection
	)
)
if /i "%StaticCompression%" EQU "true" if %StaticCompressionRefresh% GTR 0 (
	set /A StaticCompressionReload-=1
	if "!StaticCompressionReload!" EQU "0" (
		echo;[Main Thread] Refresh Static Compression ...
		call config_tool.bat StaticCompression
		call :log [%time:~0,8%] Static Compression refreshed ...
		set /A StaticCompressionReload=%StaticCompressionRefresh%
	)
)
if not exist ..\Session goto :end
call :wait %LoopTime%
goto waitClient


:connection
echo;Socket connection accepted.
for /f "delims=$ tokens=1,2" %%A in ("%File%") do (
	set REMOTE_ADDR=%%A
	set Port=%%B
)
mkdir "..\phphtml\%REMOTE_ADDR%" 2>NUL
echo;[%REMOTE_ADDR%:%Port%] Searching client's packet.
:SearchPacket
for /l %%A in (0,1,%maxwaitcount%) do (
	if exist "!File!_OUT" if not exist "!File!_IN" goto packetfound
 	call :wait %maxwaittime%
)
echo;[%REMOTE_ADDR%:%Port%] Packet request finished, packet not found.
if exist "!File!_IN" (
	echo;[%REMOTE_ADDR%:%Port%] File in reading ^(streaming probably^), waiting for terminate connection ...
	call :log [%time:~0,8%] Streaming Detected - %REMOTE_ADDR%:%Port% - Awnser's file in reading, waiting for terminate connection ...
	call :ConnectionInAttent
)
del "!File!"> nul 2>&1
echo;[%REMOTE_ADDR%:%Port%] Connection terminated
goto :eof


:ConnectionInAttent
call :wait %connectionTime%
if not exist "!File!_IN" (
	goto SearchPacket
) else if not exist "!File!" goto :EOF
goto ConnectionInAttent

REM Une potentielle requˆte est trouv‚e.
:packetfound
echo;[%REMOTE_ADDR%:%Port%] Request found.
(
	for /f "delims==" %%A in ('set HTTP ^& set PHP_HEADER') do set %%A=
	set fpath=
	set request=
	set useCompression=
	set PHP_TMP=
) 2>nul

set line=0


for /f "tokens=* delims=" %%A in (!File!_OUT) do (
	
	if "%%A"=="" goto 1_skip
	
	if !line! NEQ 0 (
		for /f "delims=: tokens=1,*" %%B in ("%%A") do set HTTP_%%B=%%C
		) else for /f "tokens=1,2,3" %%B in ("%%A") do (
			set REQUEST_METHOD=%%B
			set fpath=%%C
			set SERVER_PROTOCOL=%%D)
	set /a line+=1
)
:1_skip
for %%A in (^:) do set "HTTP_Host=!HTTP_Host:%%A=!"
if /i "!VH-%HTTP_Host:~1%!" NEQ "" (
	set "HOST=!VH-%HTTP_Host:~1%!"
	) else (
		set "HOST=!VH-Default!"
		set HTTP_Host= default
)

for %%A in (
	"%%20^%%20^ "	"%%5C^%%5C^\"	"%%B0^%%C2%%B0^�"
	"%%E0^%%C3%%A0^�" "%%E2^%%C3%%A2^�" "%%E3^%%C3%%A3^�" "%%E4^%%C3%%A4^�"
	"%%E7^%%C3%%A7^�"
	"%%E8^%%C3%%A8^�"	"%%E9^%%C3%%A9^�"
	"%%F2^%%C3%%B2^�"	"%%F3^%%C3%%B3^�"
) do for /f "tokens=1-3 delims=^" %%B in ("%%~A") do (
	set fpath=!fpath:%%C=%%D!
	set fpath=!fpath:%%B=%%D!
)	

echo;[%REMOTE_ADDR%:%Port%] %REQUEST_METHOD% Request on "!fpath!" (%HTTP_Host:~1%)

if "%REQUEST_METHOD%" == "GET" (
	call :BasicsCheck||(
		del "!File!_Out" 2>NUL
		goto SearchPacket)
	goto GET

) else if "%REQUEST_METHOD%" == "POST" (
	if /i "%HTTP_Content-Type:~1%" NEQ "application/x-www-form-urlencoded" (
		echo;[%REMOTE_ADDR%:%Port%] POST with Content-Type: %HTTP_Content-Type:~1% not implemented
		call :log [%time:~0,8%] Client Connected - %REMOTE_ADDR%:%Port% - Post with "Content-Type: %HTTP_Content-Type:~1%" is not implemented
		call :ErrorSend "501" "Post with Content-Type: %HTTP_Content-Type:~1%"
		goto SendReponse)
	call :BasicsCheck||(
		del "!File!_Out" 2>NUL
		goto SearchPacket)
	goto POST
) else (
	echo;[%REMOTE_ADDR%:%Port%] %REQUEST_METHOD% Request is not supported
	call :ErrorSend "501" "%REQUEST_METHOD%"
	goto SendReponse
	)
if exist "!File!_Out" del "!File!_Out"
call :wait %connectionTime%
if exist "!File!_OUT" goto packetfound
goto :eof

:end
call :log [%time:~0,8%] SockeT.exe closed ^^(or crashed^^), server stopped
echo;SockeT was closed, session ends.
echo;The session ends.
pause
exit

:genSockeT
pushd SockeT_src\build_scripts
echo Build minimal SockeT Reloaded Server
(
	cmd /c build_server_min_compat.bat <NUL
	move bin\SockeT.exe ..\..
)> nul 2>&1
popd
if not exist SockeT.exe exit /b 1
exit /b 0

:reset
echo Cleanup SockeT...
(
	del Session
	del /S /Q SockeT
	rmdir SockeT
)>nul 2>nul

call :log [%time:~0,8%] Cleanup SockeT
goto :eof


:: Send an http response to the IP
REM Basically OK, change it when there was an error
:BasicsCheck
set rep=200

for /f "tokens=1,* delims=?" %%G in ("!fpath!") do (
	set fpath=%%G
	set "QUERY_STRING=%%H"
)

for %%L in (^| ^> ^< ^& %%) do set "fpath=!fpath:%%L=!"
set "fpath=!fpath:)=^)!"
set "fpath=!fpath:(=^(!"

set isDirectory=false
if "!fpath!" NEQ "/" (
	if "!fpath:~0,1!"=="/" set fpath=!fpath:~1!
	(pushd "!HOST!\!fpath:/=\!" 2>NUL)&&(
	popd
	set isDirectory=true)
) else if exist "!HOST!\index.php" (set fpath=index.php) else set fpath=index.html

set fpath=!fpath://=/!
set fpath=!fpath:/=\!
set PATH_TRANSLATED=!HOST!\!fpath!

for %%A in (%fpath:\= %) do if /i "%%~A" EQU ".." goto Error403

if "%isDirectory%" EQU "true" if "!fpath:~-1!" NEQ "\" goto 3_skip
for /f "tokens=*" %%A in ("%PATH_TRANSLATED%") do (
	if exist "%%~dpAaccess.properties" for /f "tokens=*" %%D in ("!fpath!") do for /f "tokens=* usebackq" %%B in ("%%~dpAaccess.properties") do (
		if /i "%%~B" EQU "%%~nxD" (goto Error403) else if /i "%%~B" EQU "*" (goto Error403) else if /i "%%~B" EQU "." if /i "%%~nxD" EQU "" (goto Error403)
))
:3_skip

if not exist "!HOST!\!fpath!" (
	echo;[%REMOTE_ADDR%:%Port%] File Not found
	set rep=404
	if /i "!fpath!" NEQ "favicon.ico" (
		if /i "!fpath!" EQU "index.html" (call :ErrorSend "Infos" "!fpath!") else (
			if /i "%UseDefaultErrorFile%" EQU "true" (call :ErrorSend "404" "!fpath!") else call :ExistErrorPagesDirectory "404"||(
				echo;[%REMOTE_ADDR%:%Port%] Default Error page charged
				call :ErrorSend "404" "!fpath!"
			)
		)
	)
)
echo;[%REMOTE_ADDR%:%Port%] PATH = !fpath!
exit /b 0

:Error403
echo;[%REMOTE_ADDR%:%Port%] Access denied
set rep=403
if /i "%UseDefaultErrorFile%" EQU "true" (call :ErrorSend "403" "!fpath!") else call :ExistErrorPagesDirectory "403"||(
	echo;[%REMOTE_ADDR%:%Port%] Default Error page charged
	call :ErrorSend "403" "!fpath!"
)
exit /b 0


:GET
if "%isDirectory%"=="true" if "%rep%" NEQ "403"  (
	if "!fpath:~-1!"=="\" (
		call :MakeListDir
		set type=.html
	) else (
		set location=
		For /f "tokens=*" %%A in ("!fpath!") do (
			call :ConvertCharset "location" "%%~nA/"
		)
		set rep=303
	)
	goto SendReponse
)
for /f "tokens=*" %%A in ("!fpath!") do (
	if /i "%%~xA"==".php" (
		set CONTENT_LENGTH=
		set CONTENT_TYPE=
		set SCRIPT_FILENAME=!HOST!/!fpath!
		set DOCUMENT_ROOT=!HOST!
		set SCRIPT_NAME=!fpath!
		for /f "tokens=1,* delims=:" %%B in ('..\php\php-cgi.exe ^^^^^| PhpTool.exe h "..\phpHtml\%REMOTE_ADDR%\%HTTP_Host%-%%~nA.html"') do (
			set PHP_HEADER_%%~B=%%~C
		)
		popd
		set fpath=..\phpHtml\%REMOTE_ADDR%\!HTTP_Host!-%%~nA.html
		set type=.html
	) else (
	set type=%%~xA
	set fpath=!HOST!\!fpath!
))
goto SendReponse



:ExistErrorPagesDirectory
for %%A in (php html) do if exist "%ErrorPagesDirectory%\%~1.%%A" (
	set fpath=%ErrorPagesDirectory%\%~1.%%A
	exit /b 0)
exit /b 1



:POST
If /i "%rep%" NEQ "200" (
	set fpath=!HOST!\!fpath!
	goto SendReponse
	)
for /f "skip=%line% tokens=*" %%A in (..\SockeT\!File!_OUT) do (
	set "POST=%%A"
	if /i "%HTTP_Content-Length%" EQU "" (
		set /p "tmp_post=%%A"< NUL | PhpTool l | set /p CONTENT_LENGTH=
		set tmp_post=
		) else set CONTENT_LENGTH=!HTTP_Content-Length:~1!
	)
	set CONTENT_TYPE=%HTTP_Content-Type:~1%
	set SCRIPT_FILENAME=!HOST!/!fpath!
	set DOCUMENT_ROOT=!HOST!
	set SCRIPT_NAME=!fpath!
	
	for %%L in (^| ^> ^< ^& %%) do set "POST=!POST:%%L=^^^%%L!"

		for /f "tokens=*" %%A in ("!fpath!") do (
			for /f "tokens=1,* delims=:" %%B in ('echo;!POST! ^| ..\php\php-cgi.exe ^| PhpTool.exe h "..\phpHtml\%REMOTE_ADDR%\%HTTP_Host%-%%~nA.html"') do (
				if /i "%%~C" NEQ " " set PHP_HEADER_%%~B=%%~C
			)
		popd
		set fpath=..\phpHtml\%REMOTE_ADDR%\!HTTP_Host!-%%~nA.html
		set type=.html
	)
goto SendReponse

:: Ecriture de la réponse et envoie
:SendReponse
if /i "%AllowGzip%" EQU "true" if "!rep:~0,1!" NEQ "3" (
	if /i "%GzipFormatsAccepted%" NEQ "" (
		for %%A in (%GzipFormatsAccepted%) do if /i "!type!" EQU "%%~A" call :compress&goto 2_skip
		) else call :compress
)
:2_skip
(echo;HTTP/1.1 !rep! !code_%rep%!

echo;Server: HttpBat Master ^(Support PHP, Multi-threading^)
echo;Developpers: TSnake41 and Xenoxis
echo;Host: %SERVER_NAME%
echo;Accept-Ranges: none
if "!rep:~0,1!" NEQ "3" (
	if "%useCompression%"=="true" echo;Content-Encoding: gzip
	for /F "tokens=*" %%A in ("!fpath!") do echo;Content-Length: %%~zA
	for %%A in (%CacheControlFormats%) do if /i "%type%" EQU "%%~A" echo;Cache-Control: max-age=!CacheControlMaxAge!
	if /i "%MimeIndex%" EQU "true" (
		if /i "!MIME-%type%!" NEQ "" (
			echo;Content-Type: !MIME-%type%!
			) else echo;Content-Type: octet-stream) else (
	set hasmime=0
	for /f "tokens=1,*" %%C in (..\mime.txt) do (
		if "!type!"=="%%C" (
			echo;Content-Type: %%D
			set hasmime=1
			))
	if !hasmime! EQU 0 echo;Content-Type: octet-stream
)) else (
	echo;Location: !location!
	echo;Content-Length: 0
)
for /f "tokens=1,* delims==" %%A in ('set PHP_HEADER 2^>NUL') do (
	set PHP_TMP=%%A
	echo;!PHP_TMP:~11!:%%B
	)
echo.
type "!fpath!" 2>NUL
)> ..\SockeT\!File!_IN
call :log [%time:~0,8%] Client Connected - %REMOTE_ADDR%:%Port% - !HTTP_Host! - %REQUEST_METHOD%[%rep%] on !fpath!
cd ..\Socket
del "!File!_OUT" >NUL 2>&1
goto SearchPacket



:MakeListDir
set fpath=!fpath:~0,-1!
(
	call :ConvertCharset "TmpString" "!HTTP_HOST!/!fpath:\=/!"
	for %%A in (
	"<html><head><title>HttpBat - Directory : !TmpString!</title><meta charset='Windows-1252'></head><body><h2><u>HttpBat Explorer - !TmpString!</h2></u><ul style="list-style-type:square">"
	"<li><a href="..">..</a></li>"
	) do (echo;%%~A)
	for /f "tokens=*" %%A in ('dir "!HOST!\!fpath!" /b /o:GN') do (
		call :ConvertCharset "TmpString" "%%~A"
		echo;^<li^>^<a href="!TmpString!"^>!TmpString!^</a^>^</li^>
		)
echo;^</ul^>^</body^>^</html^>
)> "..\phpHtml\%REMOTE_ADDR%\Dir_!HTTP_HOST!_!fpath:\=-!.html"
set fpath=..\phpHtml\%REMOTE_ADDR%\Dir_!HTTP_HOST!_!fpath:\=-!.html
set TmpString=
goto :EOF

:compress
	for %%A in (%HTTP_Accept-Encoding%) do (
		if /i "%%A" EQU "gzip" goto Compress_Gzip
	)
goto :eof

:Compress_Gzip
echo;[%REMOTE_ADDR%:%Port%] Compression [Gzip] is accepted by the client

set useCompression=true
set f2=!fpath!

for %%A in ("!f2!") do (
	set te=!HTTP_Host!_%%~nA
	set fpath=..\Compress\compressGzip_!te:\=_!.gz
	set te=
)

(
if /i "%StaticCompression%" NEQ "true" (
	if exist "!fpath!" del /s /q "!fpath!"
	7z a -tgzip -mx=!7Z-LevelCompression! -mfb=!7Z-FastBytes! -mpass=!7Z-Passes! "!fpath!" "%f2%"
) else (
	for /f "tokens=*" %%A in ("%f2%") do (
		if /i "%%~tA" NEQ "!FCOMP-INDEX-%HTTP_Host:~1%-%%~nxA!" (
				if exist "!fpath!" del /s /q "!fpath!"
					7z a -tgzip -mx=!7Z-LevelCompression! -mfb=!7Z-FastBytes! -mpass=!7Z-Passes! "!fpath!" "%f2%"
				) else if not exist "!fpath!" 7z a -tgzip -mx=!7Z-LevelCompression! -mfb=!7Z-FastBytes! -mpass=!7Z-Passes! "!fpath!" "%f2%"
	)
))>NUL 2>&1
goto :EOF

:ErrorSend
if not exist "..\ErrorPages\%REMOTE_ADDR%" mkdir "..\ErrorPages\%REMOTE_ADDR%"
set fpath=%REMOTE_ADDR%\%~1.html
set HOST=..\ErrorPages
call :ConvertCharset "TempStr" "%~2"
if "%~1" EQU "403" (
		for %%a in (
			"<html><head><title>HttpBat - Error %~1</title><meta charset='Windows-1252'></head><body><center>"
			"<p><u><h1>[HttpBat] - Error %~1</h1></u></p>"
			"<p><h3>You don't have right to access to !TempStr! ^^^!</h3></p>"
			"</body></center></html>") do echo;%%~a)> "..\ErrorPages\%REMOTE_ADDR%\%~1.html"&goto :EOF
if "%~1" EQU "404" (
		for %%a in (
			"<html><head><title>HttpBat - Error %~1</title><meta charset='Windows-1252'></head><body><center>"
			"<p><u><h1>[HttpBat] - Error %~1</h1></u></p>"
			"<p><h3>!TempStr! is not found on the server ^^^!</h3></p>"
			"</body></center></html>") do echo;%%~a)> "..\ErrorPages\%REMOTE_ADDR%\%~1.html"&goto :EOF
if "%~1" EQU "501" (
		for %%a in (
			"<html><head><title>HttpBat - Error %~1</title><meta charset='Windows-1252'></head><body><center>"
			"<p><u><h1>[HttpBat] - Error %~1</h1></u></p>"
			"<p><h3>!TempStr! request is not supported ^^^!</h3></p>"
			"</body></center></html>") do echo;%%~a)> "..\ErrorPages\%REMOTE_ADDR%\%~1.html"&goto :EOF
if /i "%~1" EQU "Infos" (
		call InfoPage.bat
	)> "..\ErrorPages\%REMOTE_ADDR%\%~1.html"
goto :EOF


:: Wait [ms]
:wait
if "%usebatbox%" EQU "true" (
batbox /w %~1
goto :EOF
) else Wait %~1
goto :EOF

:log
(echo;%*)>> "%basedir%\Logs\log_%date:/=-%.txt"
goto :eof

:ConvertCharset
set %~1=%~2
for %%A in (
	"�=�" "�=�"	"�=�"	"�=�"	"�=�"	"�=�"	"�=�"	"�=�"	"�=�"	"�=�"	"�=�"	"�=�"	"�=�"	"�=�"	"�=�"	"�=�"	"�=�"
	"�=�"	"�=�"	"�=�"
) do (
	set %~1=!%~1:%%~A!
)
goto :EOF


:makebatbox
(
echo;TVqAAAEAAAAEABAA//8AAEABAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAA4fug4AtAnNIbgBTM0hVGhpcyBwcm9ncmFtIGNhbm5vdCBiZSBydW4gaW4gRE9TIG1vZGUuDQokAAAAAAAAAABQRQAATAEBAGOZxlMAAAAAAAAAAOAADwELAQFGAAYAAAAGAAAAAAAAABAAAAAQAAAAEAAAAABAAAAQAAAAAgAAAQAAAAAAAAAEAAAAAAAAAAAgAAAAAgAA9ugAAAMAAAAAEAAAABAAAAAAAQAAAAAAAAAAABAAAAAAAAAAAAAAAFUTAABzAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC5mbGF0AAAA/AUAAAAQAAAABgAAAAIAAAAAAAAAAAAAAAAAAGAAAOAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABo2BVAAGoAaNgVQABozBVAAGjIFUAA/xV0FUAAavX/FVQUQACj1BVAAGr2/xVUFEAAo9AVQADomgIAAEOKE4DKIDH/ZrkLADqXSBNAAA+EeQIAAEdn4vDp2v///+hXAgAAUP811BVAAP8VXBRAAOnD////gHsBXw+FDgAAAP8VfBVAAIXAD4Sr/////xVwFUAAPeAAAAAPhQsAAAD/FXAVQAAF/wAAAFD/FXgVQADoBQIAAAMFBxNAACX//wAAo9wVQADo8AEAAAMFCxNAAMHgEAEF3BVAAP813BVAAP811BVAAP8VYBRAAOlI////6MUBAADHBQ8TQAAAAAAAow8TQABoDxNAAGj1EkAA/xVsFUAA6R/////ouQEAAFNo9RJAAP8VbBVAAOkJ////ahj/NdAVQAD/FVgUQABo2BVAAGoBaOAVQAD/NdAVQAD/FWQUQACDPdgVQAAAD4TN////ZoM94BVAAAIPhb/////3BfAVQAD9////D4Wv////iz3kFUAAif7B7xCB5v//AACh6BVAAIXAD4SR////AwXwFUAAUFdWaPgSQAD/FWwVQADpgf7//2oY/zXQFUAA/xVYFEAAaNgVQABqAWjgFUAA/zXQFUAA/xVkFEAAiz3kFUAAif7B7xCB5v//AACh6BVAAFBXVmj4EkAA/xVsFUAA6TL+///orwAAAFD/FWgUQADpIf7//+ieAAAAowcTQADolAAAAKMLE0AA6Qj+//9o9BVAAP811BVAAP8VcBRAAOh0AAAAo/gVQABo9BVAAP811BVAAP8VbBRAAOnX/f//6FQAAACJx/8VdBRAAFdQ/xUQFEAA6b39///oOgAAAEBo2BVAAFD/NdQVQAD/FXgUQADpoP3//+g6AAAAgDsAdQW7AAAAAGgCAAIAagBT/xXwE0AA6X79///oGAAAAGoAagBT/xWAFUAAg8QMw8HnAv+nFBNAAIMFzBVAAASLHcwVQACLG4XbD4QBAAAAw2oA/xV4FUAAJXMAJWQ6JWQ6JWQAJWQ6JWQAAAAAAAAAAAAAAAAAAHMQQACuEEAA7hBAAFwQQAAXEUAALRFAALURQAAVEkAABBJAAC4SQACWEkAAXxJAAHkSQABrZ2FjZG15b3doc3BmUBUAAAAAAAAAAAAAuRMAAGwVAAAoFAAAAAAAAAAAAADEEwAAVBQAAAgUAAAAAAAAAAAAANITAAAQFAAA6BMAAAAAAAAAAAAA3hMAAPATAAAAAAAAAAAAAAAAAAAAAAAAAAAAAE1TVkNSVC5ETEwAS0VSTkVMMzIuRExMAABVU0VSMzIuRExMAABXSU5NTS5ETEwA+BMAAAAAAAD4EwAAAAAAAAAAUGxheVNvdW5kQQAAAAAYFAAAAAAAABgUAAAAAAAAAABTaG93V2luZG93AAAAAIAUAACQFAAAohQAALwUAADYFAAA7BQAAPQUAAAMFQAAJBUAADgVAAAAAAAAgBQAAJAUAACiFAAAvBQAANgUAADsFAAA9BQAAAwVAAAkFQAAOBUAAAAAAAAAAEdldFN0ZEhhbmRsZQAAAABTZXRDb25zb2xlTW9kZQAAAABTZXRDb25zb2xlVGV4dEF0dHJpYnV0ZQAAAFNldENvbnNvbGVDdXJzb3JQb3NpdGlvbgAAAABSZWFkQ29uc29sZUlucHV0QQAAAFNsZWVwAAAAU2V0Q29uc29sZUN1cnNvckluZm8AAAAAR2V0Q29uc29sZUN1cnNvckluZm8AAAAAR2V0Q29uc29sZVdpbmRvdwAAAABTZXRDb25zb2xlRGlzcGxheU1vZGUAiBUAAJIVAACcFQAArBUAALQVAAC+FQAAAAAAAIgVAACSFQAAnBUAAKwVAAC0FQAAvhUAAAAAAAAAAHByaW50ZgAAAABfZ2V0Y2gAAAAAX19nZXRtYWluYXJncwAAAGV4aXQAAAAAX2tiaGl0AAAAAHN0cnRvbAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=
)>>out.file
(certutil -f -decode "out.file" "batbox.exe")>NUL
Del out.file>NUL
goto :EOF

:makeWait
del out.file 2>NUL
(
echo;TVqQAAMAAAAEAAAA//8AALgAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAA4fug4AtAnNIbgBTM0hVGhpcyBwcm9ncmFtIGNhbm5vdCBiZSBydW4gaW4gRE9TIG1vZGUuDQ0KJAAAAAAAAABQRQAATAECAAAAAAAAAAAAAAAAAOAADwMLAQYAAAAAAAAAAAAAAAAALBAAAAAQAAAAIAAAAABAAAAQAAAAAgAABAAAAAAAAAAEAAAAAAAAAAAwAAAAAgAAmg0AAAMAAAAAABAAABAAAAAAEAAAEAAAAAAAABAAAAAAAAAAAAAAAAAgAAA8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8IAAALAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC50ZXh0AAAAqAEAAAAQAAAAAgAAAAIAAAAAAAAAAAAAAAAAACAAAGAuZGF0YQAAACABAAAAIAAAAAIAAAAEAAAAAAAAAAAAAAAAAABAAADAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABVieWB7AAAAACQU4tFDIPABIsIUehHAQAAg8QEUOhGAQAAuAAAAABbycMAAFWJ5YHsLAAAAJBTjUXoUOjlAAAAg8QEuAEAAABQ6CIBAACDxAS4AAADAFC4AAABAFDoFgEAAIPECLgAAAAAiUXUjUXUUIsFHCFAAFCNRdhQjUXcUI1F5FDo9wAAAIPEFItF2FCLRdxQi0XkUOhj////g8QMiUXgi0XgUOjcAAAAg8QEW8nDAACHLCRVjWwkBFGJ6YHpABAAAIUBLQAQAAA9ABAAAH3sKcGFAYngicyLCP9gBItF7MPo9////4sAiwDD6O3///9Q6Ov///9Q6JEAAACDxAjDi2Xo6Nn///9Q6IcAAAAAAAD/////7hBAAAMRQADpewAAAFWLbCQIjUQkDIlFADHAiUUEZKEAAAAAiUUIuCARQACJRQy4FBFAAIlFEDHAiUUUjUUIZKMAAAAAXcMA/yU8IEAAAAD/JWAgQAAAAP8lQCBAAAAA/yVEIEAAAAD/JUggQAAAAP8lTCBAAAAA/yVQIEAAAAD/JVQgQAAAAP8lWCBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGggAAAAAAAAAAAAAJQgAAA8IAAAjCAAAAAAAAAAAAAABCEAAGAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJ8gAACmIAAAtyAAAMQgAADUIAAA2yAAAOkgAADxIAAAAAAAABEhAAAAAAAAnyAAAKYgAAC3IAAAxCAAANQgAADbIAAA6SAAAPEgAAAAAAAAESEAAAAAAABtc3ZjcnQuZGxsAAAAYXRvaQAAAF9fc2V0X2FwcF90eXBlAAAAX2NvbnRyb2xmcAAAAF9fZ2V0bWFpbmFyZ3MAAABleGl0AAAAX1hjcHRGaWx0ZXIAAABfZXhpdAAAAF9leGNlcHRfaGFuZGxlcjMAa2VybmVsMzIuZGxsAAAAU2xlZXAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
)>>out.file
(certutil -f -decode "out.file" "Wait.exe")>NUL
Del out.file>NUL
goto :EOF
