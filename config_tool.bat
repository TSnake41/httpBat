@echo off
REM  Copyright (C) 2016-2018 Astie Teddy (TSnake41)
REM  Copyright (C) 2016-2018 IK-DC Xenosis
REM
REM  Permission is hereby granted, free of charge, to any person obtaining
REM  a copy of this software and associated documentation files (the
REM  "Software"), to deal in the Software without restriction, including
REM  without limitation the rights to use, copy, modify, merge, publish,
REM  distribute, sublicense, and/or sell copies of the Software, and to
REM  permit persons to whom the Software is furnished to do so, subject to
REM  the following conditions:
REM
REM  The above copyright notice and this permission notice shall be
REM  included in all copies or substantial portions of the Software.
REM
REM  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
REM  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
REM  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
REM  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
REM  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
REM  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
REM  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

REM Config tool for httpBat
REM Supported functions : load, save, reset
call :%~1 2>NUL
goto :eof

:load
if not exist config.ini (
	call :reset
	)
for /f "tokens=1,* eol=# delims==" %%A in (config.ini) do (
	set %%A=%%B
)
for %%A in (IP PORT ConnectionTime MaxInstantConnections MaxWaitCount MaxWaitTime AllowGzip StaticCompression StaticCompressionRefresh LoopTime UseDefaultErrorFile MultiThreadingSupport MimeIndex CacheControlFormats CacheControlMaxAge) do (
	if not defined %%A if "%%A" NEQ "ErrorPagesDirectory" if "%%A" NEQ "ThreadPriority" if "%%A" NEQ "StaticCompression"  (
		echo;Property undefined : %%A
		echo;Press a key to reset the config file
		pause>nul
		call :reset
		goto :eof)
)
if /i "%ErrorPagesDirectory%" EQU "" (if /i "%UseDefaultErrorFile%" NEQ "true" (
		call :echo_Error "ErrorPagesDirectory" "echo;You need to define 'UseDefaultErrorFile'" "when ErrorPagesDirectory is set to false"
		set UseDefaultErrorFile=true
	)) else if /i "%UseDefaultErrorFile%" EQU "false" if "!ErrorPagesDirectory:~-1!" EQU "\" set ErrorPagesDirectory=!ErrorPagesDirectory:~0,-1!
if /i "%MultiThreadingSupport%" EQU "true" if /i "%ThreadPriority%" NEQ "LOW" if /i "%ThreadPriority%" NEQ "NORMAL" if /i "%ThreadPriority%" NEQ "HIGH" if /i "%ThreadPriority%" NEQ "REALTIME" (
		call :echo_Error "ThreadPriority" "echo;You need to define 'ThreadPriority'&echo;when 'MultiThreadingSupport' is set to 'true'"
		set ThreadPriority=NORMAL
	)
if /i "%StaticCompression%" NEQ "true" if /i "%StaticCompression%" NEQ "false" if /i "%AllowGzip%" EQU "true" (
	call :echo_Error "StaticCompression" "echo;You need to define 'StaticCompression'&echo;when 'AllowGzip' is set to 'true'"
	set StaticCompression=false
)
if /i "%AllowGzip%" EQU "true" for %%A in (7Z-LevelCompression 7Z-FastBytes 7Z-Passes) do if not defined %%A (
		echo;Property undefined : %%A
		echo;Press a key to reset the config file
		pause>nul
		call :reset
		goto :eof)

if not exist "VirtualHosts.ini" (
	call :save 1
	) else for /f "tokens=1,* delims== eol=#" %%A in (VirtualHosts.ini) do if /i "%%A" NEQ "" if /i "%%B" NEQ "" (
	set tmpstring=%%B
	set tmpstring=!tmpstring:\\=\!
	if /I "!tmpstring:~-1!" EQU "\" set tmpstring=!tmpstring:~0,-1!
	
	for %%C in (
		"$LocalDrive=%~d0"
		"$LocalDirectory=%~dp0"
		"$HTTPDirectory=%~dp0Http"
	) do for /f "tokens=1,* delims==" %%D in ("%%~C") do set tmpstring=!tmpstring:%%~D=%%~E!

	if /i "!tmpstring!" EQU "http" (set VH-%%~A=%~dp0http) else set VH-%%~A=!tmpstring!
	set VH=!VH! %%~A
)
set VH->NUL 2>&1||(
	set VH-default=%~dp0http
	set VH=default
)
set tmpstring=
goto :eof



:save
	if /i "%~1" EQU "0" (
		(
		echo;# Config file generated using the settings tool the %date% at %time%
		echo;# Please do not modify is you don't know what you're doing
		echo;
		echo;IP=!IP!
		echo;PORT=!PORT!
		echo;ConnectionTime=!ConnectionTime!
		echo;MaxInstantConnections=!MaxInstantConnections!
		echo;MaxWaitCount=!MaxWaitCount!
		echo;MaxWaitTime=!MaxWaitTime!
		echo;AllowGzip=!AllowGzip!
		echo;GzipFormatsAccepted=!GzipFormatsAccepted!
		echo;StaticCompression=!StaticCompression!
		echo;StaticCompressionRefresh=!StaticCompressionRefresh!
		echo;LoopTime=!LoopTime!
		echo;UseDefaultErrorFile=!UseDefaultErrorFile!
		echo;ErrorPagesDirectory=!ErrorPagesDirectory!
		echo;MultiThreadingSupport=!MultiThreadingSupport!
		echo;ThreadPriority=!ThreadPriority!
		echo;MimeIndex=!MimeIndex!
		echo;
		echo;CacheControlFormats=!CacheControlFormats!
		echo;CacheControlMaxAge=!CacheControlMaxAge!
		echo;
		echo;# 7z ^(Gzip compression^) options :
		echo;
		echo;7Z-LevelCompression=!7Z-LevelCompression!
		echo;7Z-FastBytes=!7Z-FastBytes!
		echo;7Z-Passes=!7Z-Passes!
	)> config.ini
) else (
	(for %%A in (
			"HttpBat Virtual Hosts"
			""
			"You can config here multiples virtual Hosts."
			"Please put entirely the PATH by respecting the following form :"
			""
			"ex : Ip(or DNS)=Path"
			""
			"By default, when it's empty, all DNS/Ip go to folder 'Http'"
			"If a request point to an non-indexed host, the request will go to"
			"the default Path ^(folder "Http"^), if you want to change the"
			"default path, set an virtual host called "default" with a specific"
			"path."
			""
			"You can define a PATH with values relatives to HttpBat, to do it"
			"just add a following alias in the PATH's setting."
			"		Driver's letter : $LocalDrive"
			"		Httpbat's Directory : $LocalDirectory"
			"		Httpbat's Http Directory : $HTTPDirectory"
			""
			"The following example define the default PATH to a directory called"
			"'New Directory' in the root of drive where is situated Httpbat."
			"ex : default=$LocalDrive\New Directory"
		) do echo;# %%~A
	)> VirtualHosts.ini
)
goto :eof


:StaticCompression
for /f "tokens=2,* delims==-" %%A in ('set VH- 2^>NUL') do (
	set TMP_TYPEOFFILE=
	for %%M in (%GzipFormatsAccepted%) do set "TMP_TYPEOFFILE=!TMP_TYPEOFFILE!"%%~B\*%%~M" "
		for /f "tokens=*" %%C in ('dir /B /A:-D /S !TMP_TYPEOFFILE!') do (
		set /a TMP_NBRFILES+=1
		set FCOMP-INDEX-%%~A-%%~nxC=%%~tC
))
if !TMP_NBRFILES! GEQ 100 (
	echo;WARNING : !TMP_NBRFILES! files in virtuals hosts. You should disable Static Compression ^^!
	echo;WARNING : A too high number of files can be reduce performance of httpbat ^^!
)
for /f "tokens=1 delims==" %%A in ('set TMP_ 2^>NUL') do set %%~A=
goto :EOF



:reset
call :default_values
call :save 0
goto :eof

:default_values
for %%A in (
	"IP=127.0.0.1"
	"PORT=80"
	"ConnectionTime=1000"
	"MaxInstantConnections=20"
	"MaxWaitCount=60"
	"MaxWaitTime=30"
	"AllowGzip=true"
	"GzipFormatsAccepted=.html;.txt;.css;.exe;.bat;.vbs"
	"StaticCompression=true"
	"StaticCompressionRefresh=6000"
	"LoopTime=50"
	"UseDefaultErrorFile=true"
	"ErrorPagesDirectory="
	"MultiThreadingSupport=true"
	"ThreadPriority=NORMAL"
	"MimeIndex=true"
	"CacheControlFormats=.ogg;.mp4;.m4a;.mp2;.mp3;.avi;.flac;.wav;.mkv;.mpeg;.aif;.jpg;.jpeg;.png;.bmp"
	"CacheControlMaxAge=172800"
	"7Z-LevelCompression=5"
	"7Z-FastBytes=32"
	"7Z-Passes=1"
) do set %%A
goto :eof

:echo_Error
echo;
echo;Property undefined or uncorrect : %~1
echo;
%~2
echo;
echo;
echo;Press a key to define %~1 to default value
pause>NUL
goto :EOF