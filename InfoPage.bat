@echo off
REM  Copyright (C) 2016-2018 Astie Teddy (TSnake41)
REM  Copyright (C) 2016-2018 IK-DC Xenosis
REM
REM  Permission is hereby granted, free of charge, to any person obtaining
REM  a copy of this software and associated documentation files (the
REM  "Software"), to deal in the Software without restriction, including
REM  without limitation the rights to use, copy, modify, merge, publish,
REM  distribute, sublicense, and/or sell copies of the Software, and to
REM  permit persons to whom the Software is furnished to do so, subject to
REM  the following conditions:
REM
REM  The above copyright notice and this permission notice shall be
REM  included in all copies or substantial portions of the Software.
REM
REM  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
REM  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
REM  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
REM  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
REM  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
REM  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
REM  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

REM Info page

for %%A in (
		"<html><style>"
		".menu {"
		"position: absolute; width: 160px; left: 10px; top: calc(50%% - 150px);  height: 300px;  border: 4px black solid;"
		"border-radius: 8px; background-color: silver;}"
		".buttonmenu {"
		"border: 2px black solid; border-radius: 18px 6px 18px 6px; background-color: white; width: 80%%; margin: 10px auto 10px auto;"
		"padding: 0px 0px 0px 10px; text-align: center; font-family: 'Georgia', 'Verdana';}"
		"a, a:link, a:visited, a:active {color: black; text-decoration: none;}"
		"a:hover {color: red;}"
		".buttonmenu:link {color: black; text-decoration: none; }"
		".p {border: 4px black solid; width: 30%%; margin-left: auto; margin-right: auto; text-align: center;}"
		".body {"
		"position: relative; width: 65%%; left: calc(160px + 150px); height: 65%%; min-height: 300px; top: calc(65%% / 5);"
		"border: 6px black solid; border-radius: 15px 10px 15px 10px;"
		"padding: 20px 50px 20px 50px; text-align: center; background-color: white;overflow: auto;}"
		"nav {text-align: center;}"
		"body {background-color: #808080;}"
		"</style><head><title>HttpBat - Information Page</title></head>"
		"<nav><div class='menu'>"
		"<a href='.'><div class='buttonmenu'>Server Infos</div></a>"
	) do (echo;%%~A)
	for /f "tokens=*" %%A in (
		"<a href='./?page=1'><div class='buttonmenu'>About Server Config</div></a><a href='./?page=2'><div class='buttonmenu'>About HttpBat</div></a><div style='position: absolute; bottom: 10px; width: 100%%;'><hr><b><u>HttpBat</u></br>Bacth Web Server</br>Config / Debug</b></br><i>Design : v2.2</i></div></div></nav><div class='body'>"
	) do (echo;%%~A)
		
if /i "%QUERY_STRING:~0,5%" EQU "page=" (
	for /l %%A in (1,1,2) do if /I "%QUERY_STRING%" EQU "page=%%A" (
		call :INFO_%%A "%~1"
		set FOUND=1
		)
	if /i "!FOUND!" NEQ "" (
		set FOUND=
		goto :EOF
	)
)
	for %%A in (
			"<p><u><h1>HttpBat - Informations</h1></u></p>"
			"<p><h4>If you get this page it's beacause you probably don't have <u>index.html</u> or <u>index.php</u> in the virtual host directory</u></h4>Create one of them files will don't display anymore this web page.</p>"
			"<p><i>Please check if there no <b>.txt extension or else</b> behind your files.</i></br>By default, Windows do not display known extensions, so be aware of it.</br>In the case that httpbat will not find your files, please check if there's no multiples extensions.</p>"
			"<hr><p><u><h4>Server Config :</h4></u>"
			"IP = %SERVER_NAME% : %SERVER_PORT%</br>"
			"Local connection = %REMOTE_ADDR% : %Port%</br>"
			"Max clients = %maxInstantConnections%</br>"
			"Gzip Allowed = %AllowGzip%</br>"
			"Gzip Formats Accepted = %GzipFormatsAccepted%</br>"
			"Static Compression = %StaticCompression%</br>"
			"Mime Index = %MimeIndex%</br>"
			"Registered virtuals host = %SERVER_NAME% %VH%</br></br>"
			"Multi-threading : %MultiThreadingSupport% | %ThreadPriority%</br>"
			"Exe : %ActuPATH%</p>"
			"<p><u><h4>Computer infos :</h4></u>"
			"OS = %OS%/%WinOs% %OsBits% bits</br>"
			"CPU = %CPU%</br>"
			"CPU Cores = %NUMBER_OF_PROCESSORS%</br></p>"
			"<hr><p><u><h4>Computer variables (ALL) :</h4></u>"
			) do (echo;%%~A)
for /f "tokens=* delims=" %%A in ('SET 2^>NUL') do (echo;%%~A^</br^>)
echo;^</br^>^</br^>
goto :EOF





:INFO_1
for %%A in (
	"<style>li {margin-bottom: 20px;}</style>"
	"<p><u><h1>How to configure HttpBat</h1></u></p>"
	"<p>HttpBat have lots of options in config.ini, they can be incomprehensible for unexperienced users</br>There's a tutorial to learn about how to configure with safely and performance HttpBat</p><hr>"
	"<p>A config.ini file with default values should look like this :</br>"
	"<div style='text-align: left;color: green;'><code># Config file generated using the settings tool the 01/01/1970 at 00:00:00,00</br># Please do not modify is you don't know what you're doing</br></br>"
	"IP=127.0.0.1</br>PORT=80</br>ConnectionTime=1000</br>MaxInstantConnections=20</br>MaxWaitCount=60</br>MaxWaitTime=30</br>AllowGzip=true</br>GzipFormatsAccepted=.html;.txt;.css;.exe;.bat;.vbs</br>StaticCompression=true</br>StaticCompressionRefresh=6000</br>LoopTime=50</br>UseDefaultErrorFile=true</br>ErrorPagesDirectory=</br>MultiThreadingSupport=true</br>ThreadPriority=NORMAL</br>MimeIndex=true</br>CacheControlFormats=.ogg;.mp4;.m4a;.mp2;.mp3;.avi;.flac;.wav;.mkv;.mpeg;.aif;.jpg;.jpeg;.png;.bmp</br>CacheControlMaxAge=172800</br></br></br># 7z (Gzip compression) options :</br></br>7Z-LevelCompression=5</br>7Z-FastBytes=32</br>7Z-Passes=1</code></div></p></br>"
	"<u>Let's detail configurable options :</u></br><ul>"
	"<div style='width: 70%%; text-align: left; margin: auto;'>"
) do echo;%%~A

for %%A in (
	"IP							Set the ip server's address for listening http requests with the <b>PORT</b>, normally you should set it to the local address of the your computer. By default, <b>IP</b> is set to <u>127.0.0.1</u>, which refers to your PC, but without listening on the network (only you can access, can be interesting for creating website safely). You can set the ip to 0.0.0.0 to listning all adress which refers to your PC"

	"PORT						Define the <b>PORT</b> used by HttpBat, whose the value's range is between 1 and 65535. Conventionally, the <b>PORT</b> for http (web) is 80, this is the default value"

	"ConnectionTime				This value is used to check connections like Streaming. The value is in milliseconds (ms), so the checking actions which used <b>ConnectionTime</b> are paused for <b>ConnectionTime</b> milliseconds"

	"MaxInstantConnections		Set the max number of connections simultaneously on the server. A high value with a high traffic may decrease performances. If there are more requests than <b>MaxInstantConnections</b>, last-arrival requests are buffered"

	"MaxWaitCount 				An internal value, which used for check disconnection of potential user with <b>MaxWaitTime</b>"

	"MaxWaitTime				Another internal value, used with <b>MaxWaitCount</b> to get the timeout for check disconnection of potential user. To get timeout's time : <b>MaxWaitCount</b> by <b>MaxWaitTime</b>"

	"AllowGzip					Allow Httpbat to compress request before send. So active this option will permit to use less bandwidth, but it'll may use more CPU"

	"GzipFormatsAccepted		File extensions which will be compressed if <b>AllowGzip</b> is set to <u>true</u>. For define news formats, you need to write a dot followed directly by the file's extension, and separate every string by a semicolon or a space. The compression has no effects on final file's size (see a negative one) on multimedia files, like video, picture and music"

	"StaticCompression			Allow Httpbat to not re-compress the same file on every requests if it wasen't modified, by making a file index. Can save CPU on website with lot of traffic"

	"StaticCompressionRefresh	Refresh the file index of Static Compression. Multiplied by <u><b>LoopTime</b> x 10^-3</u>, it give the time interval of every index refresh. If a value less or equal to zero is gived, no refresh of index will be executed"

	"LoopTime					Internal value, define the main loop time pause in milliseconds. The main loop detect every new connection. Discrease this value may increase CPU usage, but the server will awnser more quickly (unless the CPU is too much used)"

	"UseDefaultErrorFile		Define if Httpbat use default error messages or not. In false anwsered case, the folder where new errors messages will be <b>ErrorPagesDirectory</b>"

	"ErrorPagesDirectory		Set the folder of errors messages. Errors messages files should be .html or .php files with name corresponding to the specific error (ex : '404.html')"

	"MultiThreadingSupport		Define if Httpbat will use multiple threads. Can improve a lot performances, but may use more CPU. It's highly recommanded to active it. But, in some rare cases (like uni-core CPU), disable this option can improve performances"

	"ThreadPriority				Set the Httpbat threads priority for child threads used when <b>MultiThreadingSupport</b> is defined to </u>true</u>. In fact, it define the Sheduler's process priority, can take values (from lowest to hightest) : <i>LOW</i>, <i>BELOWNORMAL</i>, <i>ABOVENORMAL</i>, <i>NORMAL</i>, <i>HIGH</i>, <i>REALTIME</i>"

	"MimeIndex					Allow Httpbat to create a index of Mime types in memory, avoid parse mime.txt on every requests. May improve performances"

	"7Z-LevelCompression		Sets the level of compression, see <a href='https://sevenzip.osdn.jp/chm/cmdline/switches/method.htm#GZ'><span style='color: blue;'>7z help</span></a>"

	"7Z-FastBytes				Sets the number of fast bytes, see <a href='https://sevenzip.osdn.jp/chm/cmdline/switches/method.htm#Deflate_FastBytes'><span style='color: blue;'>7z help</span></a>"

	"7Z-Passes					Sets number of Passes, see <a href='https://sevenzip.osdn.jp/chm/cmdline/switches/method.htm#GZ'><span style='color: blue;'>7z help</span></a>"
	
	"CacheControlFormats		Define what formats will be cached by the client, Httpbat will add the HTTP's header 'Cache-Control' for only thoses files formats"
	
	"CacheControlMaxAge			Set the 'max-age' for <b>CacheControlFormats</b>, the time is in seconds, the default value is 172800 seconds (48 hours)"
) do for /f "tokens=1,*" %%B in ("%%~A") do for %%D in ("<li><u><b>%%~B</b> :</u> %%~C.</li>") do echo;%%~D
echo;^</div^>^</div^>
goto :EOF


:INFO_2
	for %%A in (
	"<p><u><h1>About HttpBat</h1></u></p>"
	"<p>HttpBat is a project launched in 2016 by <b>TSnake41</b> and <b>Xenoxis</b></br>Httpbat offers the best of batch in terms of performance, safely and lightness</p>"
	"<p>Be sure to be up to date using :</br><b><u>GitLab Httpbat</b></u> : <a href='https://gitlab.com/TSnake41/httpBat/tree/master'><span style='color: blue'>click here</span></a></p>"
	"<hr><p><h2><u>External Commands :</u></h2>"
	) do (echo;%%~A)
	for /f "tokens=* delims=" %%A in (..\Commands) do (echo;%%~A^</br^>) 2>NUL
echo;^</br^>^</br^>
goto :EOF