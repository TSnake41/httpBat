/*
	PHP_Tool - HTTPBAT
	
	This tool is used for multiples utilities
	like fix a bug in PHP 5, pass headers to clients,
	and help HTTPBAT to be faster for basics tasks which
	can't be realised in batch with enought performances !
	
	Made by Xenoxis - 2017
	
	
	Permission is hereby granted, free of charge, to any person obtaining
	a copy of this software and associated documentation files (the
	"Software"), to deal in the Software without restriction, including
	without limitation the rights to use, copy, modify, merge, publish,
	distribute, sublicense, and/or sell copies of the Software, and to
	permit persons to whom the Software is furnished to do so, subject to
	the following conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
	IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
	CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
	TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
	
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE_STDIN 1024


/* Mains Functions */
void PHP(FILE*);
void POST();
void help();

/* Functions used like "Libs" */
void *CheckAlloc(void*);
void FileWrite(char*, unsigned int, FILE* Output);
FILE* OpenFile(char*);
unsigned int ReadFile(char*);



int main(unsigned int argv, unsigned char *argc[])
{
	if (argv < 2) help();
	
		switch(argc[1][0])
		{
			/* 'h' like "Headers" */
			case 'h':
				PHP(OpenFile(argc[2]));
				break;
		
			/* 'l' like "Lenght" */
			case 'l':
				POST();
				break;

			/* Undefined command */
			default:
				help();
				break;
	}
	
	/* Never reached */
	return 0;
}


void PHP(FILE* Output)
{
	unsigned char *pos, StdinBuffer[BUFFER_SIZE_STDIN + 1] = { '\0' }, lastChar[4];
	unsigned int readed = 0, reached = 0, pass=0;
	
	while ((readed = ReadFile(StdinBuffer)) > 0)
	{
		switch (reached)
		{
			case 1:
				FileWrite(StdinBuffer, readed, Output);
			break;
			
			
			default:
                if (pass == 0 && (StdinBuffer[0] == '\n' || StdinBuffer[0] == '\r'))
                {
                    FileWrite(StdinBuffer + 1, readed - 1, Output);
                    reached = 1;
                    break;
                }
                pass++;
				pos = strstr(StdinBuffer, "\n\n");
					if (pos == NULL)
					{
						pos = strstr(StdinBuffer, "\r\n\r\n");
						if (pos == NULL) {FileWrite(StdinBuffer, readed, stdout); break;}
					}
				/*if (strcmpi("\n\n", lastChar+2) == 0)
				memcpy(StdinBuffer-4, &lastChar, 4);*/
				if (strlen(pos) > 2)
				{
					FileWrite(pos+2, strlen(pos+2), Output);
					FileWrite(StdinBuffer, readed - strlen(pos), stdout);
					reached = 1;
				}
				break;
		}
	}
	fclose(Output);
	exit(0);
}

void POST()
{
	unsigned char StdinBuffer[BUFFER_SIZE_STDIN + 1] = { '\0' };
	unsigned long Totalreaded = 0;
	unsigned int readed = 0;
	
	while ((readed = ReadFile(StdinBuffer)) > 0)
	{
		Totalreaded+=readed;
	}
	printf("%ld", readed);
	free(StdinBuffer);
	exit(0);
}

void help()
{
	printf(
		"PHP_Tool - HttpBat\n\n"
		"This tool is used for multiples utilities\n"
		"like fix a bug in PHP 5, pass headers to clients,\n"
		"and help HTTPBAT to be faster for basics tasks which\n"
		"can't be realised in batch with enought performances !\n\n"
		"Made by Xenoxis - 2017\n"
	);
	exit(0);
}





/*
	"Libs" for Php Tool 
	-------------------
	
	Safer (upgraded) functions for use instantly
	with integred error-checking
	
	Best compromise between safely, speed and size
*/

FILE* OpenFile(char* Output)
{
	return CheckAlloc(fopen(Output, "wb"));
}

unsigned int ReadFile(char* Buffer)
{
	return fread(Buffer, 1, BUFFER_SIZE_STDIN, stdin);
}


void FileWrite(char *ToOutput, unsigned int readed, FILE* Output)
{
	fwrite(ToOutput, 1, readed, Output);
	return;
}


void *CheckAlloc(void* Ptr)
{
	/* If an memory allocation fail, generaly, the
	pointer will be set to NULL, to provide a better source
	code and faster executable, it will be check in only one
	function */
	if (Ptr == NULL) {exit(-1);}
	return Ptr;
}