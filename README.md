# **Httpbat Master :**


### 1. What is Httpbat ?

Httpbat is a free and open-source http server wrote in batch (which runs under cmd.exe).
It does support most of the features of web servers like Apache and Nginx :

- GET/POST methods support (Only "url-encoded" POST requests are currently supported)
- Gzip (compression) with advanced parameters
- Multi-threading
- PHP
- IP/Port change support
- Secured file access.properties
- Bugs-free and security breaches-free
- Streaming (all types of files)
- MaxClients connected to the server
- Full custom Http Error messages
- Gzip Static Compression by file extension
- Client Caching by file extension
- File Explorer (called Httpbat Explorer)
- CGI (actually only used for php)
- UTF-8 paths and URL (partial)


### 2. Performance

Apache is generally a bit quicker (only by 100-200ms), however
it depends of several parameters and in some cases Httpbat can be faster than Apache.  
It can easily be explained by the fact that Httpbat is entirely written in batch, 
while Apache is written in C (which is a lower-level programming language),  
and so cannot achieve such performances.


### 3. How to use

It's really simple ! Just run Httpbat.bat and place your website files into the "http" folder.  
Now, you can access it from any web browser at "127.0.0.1".
You'll get the information page, which provide to you informations about
your server.
In case you want to look deeper into the customization settings,
you can always take a look at the "Config.ini" configuration file. 
But please do keep in mind that Httpbat is
already configured to get the best performance on most plateforms ! :thumbsup:


### 4. What about the "Access.properties" files ?

Those are permission files, and should be placed into any "Http" folder or subfolder.
They let you restrict access to certains files on your webserver.
If you want to forbid the access to a file from the web, just write its name into the Access.properties file (using the format "name.extension").

The usage of the wildcard "*" is allowed, which will deny the access to all files and folders, even with the Httpbat Explorer.

In the case where you want just forbid the access from the Httpbat Explorer, use the dot : .

All the permission settings written into Access.properties files are not recursive (they do not affect subfolders).



## **Developers Team :**


**@TSnake41** : Initiated the project and wrote the first version of Httpbat. He's now focalized on the Httpbat Light version.

**@IKDC (Xenoxis)** : Made most of functionalities of Httpbat, like multi-threading, php and more ... He is the actual developer of Httpbat Master.